package testEntite;

// Ne pas faire un copier/coller du pdf...

// Importer les classes jdbc
import java.sql.*;
import java.util.*;

public class testEntite {
	
	static ArrayList<Etudiant> listeEtudiant = new ArrayList<Etudiant>();
	// La requete de test
	static final String req = "SELECT NUM_ET, NOM_ET, PRENOM_ET "
			+ "FROM ETUDIANT " + "WHERE VILLE_ET = 'AIX-EN-PROVENCE'";
	
	public static void main(String[] args) throws SQLException {
		// Objet materialisant la connexion a la base de donnees
		System.out.println("Connexion ");
		try (Connection conn = ConnectionUnique.getInstance().getConnection()) {
			// Connexion a la base
			System.out.println("Connecte\n");
			// Creation d'une instruction SQL
			Statement stmt = conn.createStatement();
			// Execution de la requete
			System.out.println("Execution de la requete : " + req + "\n");
			ResultSet rset = stmt.executeQuery(req);
			// Affichage du resultat
			while (rset.next()) {
				Etudiant E = new Etudiant();
				E.setNumEt(rset.getInt("NUM_ET"));
				E.setNomEt(rset.getString("NOM_ET"));
				E.setPrenomEt(rset.getString("PRENOM_ET"));
				listeEtudiant.add(E); //sois j'affiche le tableau d'etudiant, sois l'objet étudiant a chaque itération
				//System.out.println(E.toString() + "\n");
			}
			// Fermeture de l'instruction (liberation des ressources)
			stmt.close();
			System.out.println(listeEtudiant.toString() ); //afichage du tableau d'étudiants, l'affichage d'étudiants a chaque itérarion est plus lisible.
			System.out.println("\nOk.\n");
		} catch (SQLException e) {
			e.printStackTrace();// Arggg!!!
			System.out.println(e.getMessage() + "\n");
		}
	}
}
